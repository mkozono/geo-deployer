module GeoDeployer
  class Executor
    attr_reader :container_engine_command, :container_name

    def initialize(container_engine_command, container_name)
      @container_engine_command = container_engine_command
      @container_name = container_name
    end

    def call(command_array, working_dir: nil, description: nil, raise_on_failure: true)
      total_command_array = %W(#{container_engine_command} exec)
      total_command_array += %W(-w #{working_dir}) if working_dir
      total_command_array += [container_name] + command_array

      result = Shellout.new(total_command_array).execute

      raise "Error while running: #{description ? description : command_array}" if raise_on_failure && !result.success?

      result
    end
  end
end