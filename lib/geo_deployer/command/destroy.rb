module GeoDeployer
  module Command
    class Destroy
      attr_reader :config

      def initialize(config)
        @config = config

        # TODO: validate and prompt config for destroy command
      end

      def execute
        container_runner.validate_container_engine_command!

        container_runner.in_get_container do |executor|
          executor.call(%W(gcloud auth activate-service-account --key-file=#{config.container_google_application_credentials_json_path}), description: "Auth with gcloud")

          executor.call(%W(gcloud config set project #{gcp_project_id}), description: "Set gcloud project")

          executor.call(%W(mise use -g terraform@1.9.0), working_dir: '/environments/tiny.gotgeo.xyz/eu/terraform', description: "Set a terraform version for mise shim, globally")

          executor.call(%W(terraform destroy -auto-approve), working_dir: '/environments/tiny.gotgeo.xyz/us/terraform', description: "Terraform destroy the secondary site")

          executor.call(%W(terraform destroy -auto-approve), working_dir: '/environments/tiny.gotgeo.xyz/eu/terraform', description: "Terraform destroy the primary site")
        end
      end

      private

      def gcp_project_id
        config.gcp_project_id
      end

      def container_runner
        @container_runner ||= ContainerRunner.new(config)
      end
    end
  end
end