module GeoDeployer
  module Command
    class Generate
      attr_reader :config

      def initialize(config)
        @config = config

        # TODO: validate and prompt config for generate command
      end

      # Generates:
      #   1. Environment configs for GitLab Environment Toolkit
      #   2. Local configs for Geo Deployer
      #
      # By default, does not overwrite configs.
      def execute
        # TODO
      end
    end
  end
end