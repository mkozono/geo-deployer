module GeoDeployer
  module Command
    class Console
      attr_reader :config

      def initialize(config)
        @config = config
      end

      # Runs a GitLab Environment Toolkit container and opens a console in it.
      #
      # @return [void]
      def execute
        container_runner.validate_container_engine_command!

        container_runner.in_get_container do |executor|
          executor.call(%W(gcloud auth activate-service-account --key-file=#{config.container_google_application_credentials_json_path}), description: "Auth with gcloud")

          executor.call(%W(gcloud config set project #{gcp_project_id}), description: "Set gcloud project")

          binding.pry
        end
      end

      private

      def gcp_project_id
        config.gcp_project_id
      end

      def container_runner
        @container_runner ||= ContainerRunner.new(config)
      end
    end
  end
end