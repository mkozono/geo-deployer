module GeoDeployer
  module Command
    class Apply
      attr_reader :config

      def initialize(config)
        @config = config

        # TODO: validate and prompt config for apply command
      end

      def execute
        container_runner.validate_container_engine_command!

        container_runner.in_get_container do |executor|
          executor.call(%W(gcloud auth activate-service-account --key-file=#{config.container_google_application_credentials_json_path}), description: "Auth with gcloud")

          executor.call(%W(gcloud config set project #{gcp_project_id}), description: "Set gcloud project")

          executor.call(%W(ansible-playbook -i /environments/tiny.gotgeo.xyz/us/ansible -i /environments/tiny.gotgeo.xyz/eu/ansible /geo-deployer/ansible/playbooks/bootstrap.yml -v), working_dir: '/gitlab-environment-toolkit/ansible', description: "Run bootstrap.yml playbook")

          executor.call(%W(mise use -g terraform@1.9.0), working_dir: '/environments/tiny.gotgeo.xyz/eu/terraform', description: "Set a terraform version for mise shim, globally")

          executor.call(%W(terraform init -force-copy -upgrade), working_dir: '/environments/tiny.gotgeo.xyz/eu/terraform', description: "Terraform init the primary site")

          executor.call(%W(terraform apply -auto-approve), working_dir: '/environments/tiny.gotgeo.xyz/eu/terraform', description: "Terraform apply the primary site")

          executor.call(%W(terraform init -force-copy -upgrade), working_dir: '/environments/tiny.gotgeo.xyz/us/terraform', description: "Terraform init the secondary site")

          executor.call(%W(terraform apply -auto-approve), working_dir: '/environments/tiny.gotgeo.xyz/us/terraform', description: "Terraform apply the secondary site")

          # TODO: Turn on the VMs if they are off

          # This will fail the first time since SSL won't be set up but the external URL is https. Therefore we disable raise on failure, and run it again after setting up SSL.
          executor.call(%W(ansible-playbook -i /environments/tiny.gotgeo.xyz/eu/ansible playbooks/all.yml), raise_on_failure: false, working_dir: '/gitlab-environment-toolkit/ansible', description: "Configure the primary site")

          sleep(30) # Wait for primary site to be ready

          executor.call(%W(ansible-playbook -i /environments/tiny.gotgeo.xyz/eu/ansible /geo-deployer/ansible/playbooks/get_ssl_certs.yml -v), working_dir: '/gitlab-environment-toolkit/ansible', description: "Set up Let's Encrypt with gcloud API")

          executor.call(%W(ansible-playbook -i /environments/tiny.gotgeo.xyz/eu/ansible /geo-deployer/ansible/playbooks/download_ssl_certs.yml -v), working_dir: '/gitlab-environment-toolkit/ansible', description: "Copy the primary site's SSL certs")

          executor.call(%W(ansible-playbook -i /environments/tiny.gotgeo.xyz/eu/ansible playbooks/all.yml), working_dir: '/gitlab-environment-toolkit/ansible', description: "Configure the primary site")

          executor.call(%W(ansible-playbook -i /environments/tiny.gotgeo.xyz/us/ansible -i /environments/tiny.gotgeo.xyz/eu/ansible /geo-deployer/ansible/playbooks/point_dns_at_secondary_site.yml -v), working_dir: '/gitlab-environment-toolkit/ansible', description: "Point unified URL at the secondary site")

          executor.call(%W(ansible-playbook -i /environments/tiny.gotgeo.xyz/us/ansible playbooks/all.yml), working_dir: '/gitlab-environment-toolkit/ansible', description: "Configure the secondary site")

          executor.call(%W(ansible-playbook -i /environments/tiny.gotgeo.xyz/us/ansible -i /environments/tiny.gotgeo.xyz/eu/ansible playbooks/gitlab_geo.yml), working_dir: '/gitlab-environment-toolkit/ansible', description: "Configure both sites with Geo")
        end
      end

      private

      def gcp_project_id
        config.gcp_project_id
      end

      def container_runner
        @container_runner ||= ContainerRunner.new(config)
      end
    end
  end
end