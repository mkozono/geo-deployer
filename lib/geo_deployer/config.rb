module GeoDeployer
  class Config < Anyway::Config
    attr_config :container_engine_command,
      :dns_zone_name,
      :local_environments_path,
      :gcp_project_id,
      :container_geo_deployer_path,
      :container_environments_path,
      :container_gitlab_environment_toolkit_path,
      :container_name_prefix,
      :gitlab_environment_toolkit_image,
      :container_google_application_credentials_json_path,
      :local_gitlab_environment_toolkit_path,
      :keep_container,
      :primary_site,
      :secondary_site,
      :unified_host,
      :log_level

    def self.config
      return @config if defined? @config

      # https://github.com/palkan/anyway_config/tree/v2.3.1#dynamic-configuration
      loaded_config = self.for(:geo_deployer)

      @config = new(loaded_config)
    end

    def debug?
      log_level == "debug"
    end

    def unified_registry_host
      "registry.#{unified_host}"
    end
  end
end