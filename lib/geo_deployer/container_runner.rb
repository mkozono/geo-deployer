module GeoDeployer
  class ContainerRunner
    attr_reader :config

    def initialize(config)
      @config = config
    end

    def validate_container_engine_command!
      result = Shellout.new(%W{#{container_engine_command} ps}).execute
      raise "\"#{container_engine_command}\" command not available. Is it installed and running? Do you need to specify a different container_engine_command in the configs?" unless result.success?
    end

    # Run a single command in a GET container
    def execute_in_get_container(cmd_array)
      cmd = %W(#{container_engine_command} run
                 --detach
                 #{"--rm" unless config.keep_container}
                 -e GOOGLE_APPLICATION_CREDENTIALS=#{config.container_google_application_credentials_json_path}
                 --volume=#{Dir.getwd}:#{config.container_geo_deployer_path}
                 --volume=#{config.local_environments_path}:#{config.container_environments_path}
                 #{"--volume=#{config.local_gitlab_environment_toolkit_path}:#{config.container_gitlab_environment_toolkit_path}" unless config.local_gitlab_environment_toolkit_path.to_s.empty?}
                 --name=#{container_name}
                 #{config.gitlab_environment_toolkit_image}
      ) + cmd_array

      result = Shellout.new(cmd.compact).execute

      Shellout.new(%W{#{container_engine_command} ps}).execute if config.debug?

      raise "Failed to run command in GitLab Environment Toolkit container \"#{container_name}\"" unless result.success?
    end

    # Yield an Executor in a GET container
    def in_get_container
      cmd = %W(#{container_engine_command} run
                 --detach
                 #{"--rm" unless config.keep_container}
                 -e GOOGLE_APPLICATION_CREDENTIALS=#{config.container_google_application_credentials_json_path}
                 --volume=#{Dir.getwd}:#{config.container_geo_deployer_path}
                 --volume=#{config.local_environments_path}:#{config.container_environments_path}
                 #{"--volume=#{config.local_gitlab_environment_toolkit_path}:#{config.container_gitlab_environment_toolkit_path}" unless config.local_gitlab_environment_toolkit_path.to_s.empty?}
                 --name=#{container_name}
                 #{config.gitlab_environment_toolkit_image}
                 tail -f /dev/null
      )

      result = Shellout.new(cmd.compact).execute
      raise "Failed to start GitLab Environment Toolkit container \"#{container_name}\"" unless result.success?

      begin
        executor = Executor.new(container_engine_command, container_name)
        yield(executor)
      ensure
        result = Shellout.new(%W(#{container_engine_command} stop #{container_name})).execute
        raise "Failed to stop GitLab Environment Toolkit container \"#{container_name}\"" unless result.success?
      end
    end

    def container_name
      @container_name ||= "#{config.container_name_prefix}#{rand(99999999)}"
    end

    def container_engine_command
      config.container_engine_command
    end
  end
end