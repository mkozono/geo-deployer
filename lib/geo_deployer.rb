require "anyway_config"
require "pry-byebug"
require "thor"
require "zeitwerk"

loader = Zeitwerk::Loader.for_gem
loader.setup

Anyway::Settings.use_local_files = true
