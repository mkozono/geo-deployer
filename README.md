# Geo Deployer

## Requirements

- You must be an admin in the Google project where you will deploy GitLab.
- You must already have a domain registered in the same Google project. In these instructions, we will use `example.com`.
- You must have a container engine running, such as Docker Engine or Podman. This tool will run a [gitlab-environment-toolkit container](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/container_registry/2697240) to avoid having to locally install and manage its dependencies.
- You must have a Google application credentials JSON file for a service account with the permissions required by GitLab Environment Toolkit. Follow the first 4 steps of [Preparing the environment - Google Cloud Platform](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_prep.md?plain=0#google-cloud-platform-gcp) (through `4. Setup SSH Authentication`).

## Installation

Get geo-deployer:

```shell
git clone https://gitlab.com/mkozono/geo-deployer
cd geo-deployer
```

## Generate new environment configs

Generate new environment configs:

```shell
bin/geo-deployer generate
```

## Deploy an environment

Deploy an environment. The word "Deploy" looks similar to "destroy" at a glance, so the command is named apply, like terraform:

```shell
bin/geo-deployer apply
```

Some GCP resources should have been created:

- 2 cloud storage buckets
- 2 external IP addresses
- 4 DNS record sets, for example:
   - `gitlab.example.com`: The main GitLab URL, using geolocation-based DNS in which Europe is directed to the primary site located in Europe, and the rest of the world is directed to the secondary site located in the U.S.
   - `registry.gitlab.example.com`: The primary GitLab site's container registry. At the moment, there isn't a way to use the secondary site's container registry directly.
   - `eu.gitlab.example.com`: The primary site's Internal URL.
   - `us.gitlab.example.com`: The secondary site's Internal URL.

A directory named `gitlab.example.com` should have been created and populated with configs for GitLab Environment Toolkit.

GitLab Environment Toolkit should have provisioned both sites with Terraform.

GitLab Environment Toolkit should have configured both sites with Ansible, and then configured Geo.

The sites should have SSL configured. The Let's Encrypt SSL certificate should have been downloaded to `keys` directory in the `gitlab.example.com` environment directory.

## Destroy an environment

Destroy an environment:

```shell
bin/geo-deployer destroy
```